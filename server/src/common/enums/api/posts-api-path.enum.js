const PostsApiPath = {
  ROOT: '/',
  $ID: '/:id',
  REACT: '/react',
  DISREACT: '/disreact'
};

export { PostsApiPath };
